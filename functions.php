<?php
$title = "Functions";
include_once("block/header.php"); ?>

<h1 class="text-center m-3"><?php echo ($title) ?></h1>

<div class="accordion p-4" id="accordionExample">
    <div class="accordion-item">
        <h2 class="accordion-header" id="headingTen">
            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                Créer une fonction
            </button>
        </h2>
        <div id="collapseTen" class="accordion-collapse collapse" aria-labelledby="headingTen">
            <div class="accordion-body">
                <h3>Créer une fonction</h3>
                <code>
                    function calculette(string $operator, float $n1, float $n2): int|float|string <br>
                    {
                    if ($operator === "/") { <br>
                    return $n1 / $n2;<br>
                    } elseif ($operator === "*") {<br>
                    return $n1 * $n2;<br>
                    } elseif ($operator === "+") {<br>
                    return $n1 + $n2;<br>
                    } elseif ($operator === "-") {<br>
                    return $n1 - $n2;<br>
                    } else {<br>
                    return "Invalid operator";<br>
                    }<br>
                    }
                    var_dump(calculette("/", 10, 6));
                    <?php echo calculette("/", 10, 6); ?>
                    echo calculette("*", 12, 2);
                    <?php echo calculette("*", 12, 2); ?>
                    echo calculette("+", 12, 2);
                    <?php echo calculette("+", 12, 2); ?>
                    echo calculette("-", 12, 2);?>
                    <?php echo calculette("-", 12, 2); ?>
                </code>
                <?php
                // Il est possible de typer les parametres, ainsi que la valeur de retour ( void si pas de return )


                function calculette(string $operator, float $n1, float $n2): int|float|string
                {


                    if ($operator === "/") {
                        return $n1 / $n2;
                    } elseif ($operator === "*") {
                        return $n1 * $n2;
                    } elseif ($operator === "+") {
                        return $n1 + $n2;
                    } elseif ($operator === "-") {
                        return $n1 - $n2;
                    } else {
                        return "Invalid operator";
                    }
                }

                var_dump(calculette("/", 10, 6));
                var_dump(calculette("*", 10, 6));
                var_dump(calculette("+", 10, 6));
                var_dump(calculette("-", 10, 6));

                ?>
            </div>
        </div>
    </div>
    <div class="accordion-item">
        <h2 class="accordion-header" id="heading11">
            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse11" aria-expanded="false" aria-controls="collapse11">
                Créer une fonction arraySearchAll
            </button>
        </h2>
        <div id="collapse11" class="accordion-collapse collapse" aria-labelledby="heading11">
            <div class="accordion-body">
                <h3>Créer une fonction arraySearchAll</h3>
                <p>arraySearchAll(array $array, mixed $needle): array</p>
                <p>Elle devra retourner tous les indices où la valeur est présente dans le tableau
                </p>
                <p>$a = [1,2,5,1,1];</p>

                <p>arraySearchAll( $a , 1 ) renvoie [ 0, 3, 4]</p>

                <code>
                    function arraySearchAll(array $array, mixed $needle): array<br>
                    {<br>
                    $results = [];<br>
                    // Pour chaque element du tableau<br>
                    foreach ($array as $key => $value) {<br>
                    //Triple égal pour vérifier le type<br>
                    if ($value === $needle) {<br>
                    //Ajouter au tableau la clé<br>
                    array_push($results, $key);<br>
                    //Équivalent<br>
                    // $results[] = $key;<br>
                    }<br>
                    }<br>
                    return $results;<br>
                    }<br>

                    $a1 = ["pomme", "poire", "banane"];<br>
                    $a2 = [10, 15, 56, 1];<br>

                    var_dump(arraySearchAll($a1, "banane")); // []<br>
                    var_dump(arraySearchAll($a2, "1")); // [3]<br>
                </code>
                <?php
                function arraySearchAll(array $array, mixed $needle): array
                {
                    $results = [];
                    // Pour chaque element du tableau
                    foreach ($array as $key => $value) {
                        //Triple égal pour vérifier le type
                        if ($value === $needle) {
                            //Ajouter au tableau la clé
                            array_push($results, $key);
                            //Équivalent
                            // $results[] = $key;
                        }
                    }
                    return $results;
                }

                $a1 = ["pomme", "poire", "banane"];
                $a2 = [10, 15, 56, 1];

                var_dump(arraySearchAll($a1, "banane")); // []
                var_dump(arraySearchAll($a2, "1")); // [3]
                ?>

            </div>
        </div>
    </div>
    <div class="accordion-item">
        <h2 class="accordion-header" id="headingArrayFunctions">
            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseArrayFunctions" aria-expanded="false" aria-controls="collapseArrayFunctions">
                Fonctions sur les tableaux
            </button>
        </h2>
        <div id="collapseArrayFunctions" class="accordion-collapse collapse" aria-labelledby="headingArrayFunctions">
            <div class="accordion-body">
                <h3>Count</h3>
                <code>
                    $array = [1, 2, 3, 4, 5];<br>
                    $count = count($array);<br>
                    var_dump($count);
                </code>

                <?php
                $array = [1, 2, 3, 4, 5];
                $count = count($array);
                var_dump($count);
                ?>

                <h3>In Array</h3>
                <code>
                    $array = [1, 2, 3, 4, 5];<br>
                    $result = in_array(3, $array);<br>
                    var_dump($result);
                </code>

                <?php
                $result = in_array(3, $array);
                var_dump($result);
                ?>

                <h3>Array Key Exists</h3>
                <code>
                    $array = ['a' => 1, 'b' => 2, 'c' => 3];<br>
                    $result = array_key_exists('b', $array);<br>
                    var_dump($result);
                </code>

                <?php
                $array = ['a' => 1, 'b' => 2, 'c' => 3];
                $result = array_key_exists('b', $array);
                var_dump($result);
                ?>
                <h3>array_unique</h3>
                <code>
                    $array = [1, 2, 2, 3, 3, 4];<br>
                    $uniqueArray = array_unique($array);<br>
                    var_dump($uniqueArray);
                </code>

                <?php
                $array = [1, 2, 2, 3, 3, 4];
                $uniqueArray = array_unique($array);
                var_dump($uniqueArray);
                ?>

                <h3>array_search</h3>
                <code>
                    $array = ['a' => 1, 'b' => 2, 'c' => 3];<br>
                    $key = array_search(2, $array);<br>
                    var_dump($key);
                </code>

                <?php
                $array = ['a' => 1, 'b' => 2, 'c' => 3];
                $key = array_search(2, $array);
                var_dump($key);
                ?>

                <h3>array_push</h3>
                <code>
                    $array = [1, 2, 3];<br>
                    array_push($array, 4, 5);<br>
                    var_dump($array);
                </code>

                <?php
                $array = [1, 2, 3];
                array_push($array, 4, 5);
                var_dump($array);
                ?>
            </div>
        </div>
    </div>
    <div class="accordion-item">
        <h2 class="accordion-header" id="headingVariableFunctions">
            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseVariableFunctions" aria-expanded="false" aria-controls="collapseVariableFunctions">
                Fonctions sur les variables
            </button>
        </h2>
        <div id="collapseVariableFunctions" class="accordion-collapse collapse" aria-labelledby="headingVariableFunctions">
            <div class="accordion-body">
                <h3>isset</h3>
                <code>
                    $var = 10;<br>
                    var_dump(isset($var));
                </code>

                <?php
                $var = 10;
                var_dump(isset($var));
                ?>

                <h3>unset</h3>
                <code>
                    $var = 10;<br>
                    unset($var);<br>
                    var_dump(isset($var));
                </code>

                <?php
                $var = 10;
                unset($var);
                var_dump(isset($var));
                ?>

                <h3>empty</h3>
                <code>
                    $var = "";<br>
                    var_dump(empty($var));
                </code>

                <?php
                $var = "";
                var_dump(empty($var));
                ?>

                <h3>is_null</h3>
                <code>
                    $var = null;<br>
                    var_dump(is_null($var));
                </code>

                <?php
                $var = null;
                var_dump(is_null($var));
                ?>
            </div>
        </div>
    </div>

    <div class="accordion-item">
        <h2 class="accordion-header" id="headingOne">
            <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                Convertir une chaine en entier
            </button>
        </h2>
        <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
            <div class="accordion-body">
                <code>
                    $price = "2€";<br>
                    var_dump(intval($price));<br>
                </code>
                <?php
                $price = "2€";
                var_dump(intval($price));
                ?>
            </div>
        </div>
    </div>
    <div class="accordion-item">
        <h2 class="accordion-header" id="headingThree">
            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                Récupérer le type d'une variable
            </button>
        </h2>
        <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree">
            <div class="accordion-body">
                <code>
                    $price = "X";<br>
                    var_dump(floatval($price));<br>
                    $price = 1;<br>
                    var_dump(floatval($price));<br>
                    $price = 1.25;<br>
                    var_dump(floatval($price));<br>
                </code>
                <?php
                $price = "X";
                var_dump(floatval($price));
                $price = 1;
                var_dump(floatval($price));
                $price = 1.25;
                var_dump(floatval($price));
                ?>
            </div>
        </div>
    </div>
    <div class="accordion-item">
        <h2 class="accordion-header" id="headingFour">
            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                Chaine en tableau
            </button>
        </h2>
        <div id="collapseFour" class="accordion-collapse collapse" aria-labelledby="headingFour">
            <div class="accordion-body">
                <code>
                    $message = "Bonjour";<br>
                    $result = str_split($message);<br>
                    var_dump($result);
                </code>
                <?php
                $message = "Bonjour";
                $result = str_split($message);
                var_dump($result);
                ?>
            </div>
        </div>
    </div>
    <div class="accordion-item">
        <h2 class="accordion-header" id="headingFive">
            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                Tableau en chaîne de caractères
            </button>
        </h2>
        <div id="collapseFive" class="accordion-collapse collapse" aria-labelledby="headingFive">
            <div class="accordion-body">
                <code>
                    $message = ["b","o","n","j","o","u","r"];<br>
                    $result = join($message);<br>
                    var_dump($result);
                </code>
                <?php
                $message = ["b", "o", "n", "j", "o", "u", "r"];
                $result = join($message);
                var_dump($result);
                ?>
            </div>
        </div>
    </div>
    <div class="accordion-item">
        <h2 class="accordion-header" id="headingSix">
            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                Mélanger un tableau
            </button>
        </h2>
        <div id="collapseSix" class="accordion-collapse collapse" aria-labelledby="headingSix">
            <div class="accordion-body">
                <code>
                    $message = ["b","o","n","j","o","u","r"];<br>
                    $result = shuffle($message);<br>
                    var_dump($result);
                </code>
                <?php
                $message = ["b", "o", "n", "j", "o", "u", "r"];
                $result = shuffle($message);
                var_dump($result);
                ?>
            </div>
        </div>
    </div>
    <div class="accordion-item">
        <h2 class="accordion-header" id="headingSeven">
            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                Filtrer des valeurs (exemple: EMAIL)
            </button>
        </h2>
        <div id="collapseSeven" class="accordion-collapse collapse" aria-labelledby="headingSeven">
            <div class="accordion-body">
                <code>
                    $mail = "joé@.com";<br>
                    $mail2 = "joé@gmail.com";<br>
                    $result = filter_var($mail, FILTER_VALIDATE_EMAIL);<br>
                    var_dump($result);<br>
                    $result = filter_var($mail2, FILTER_VALIDATE_EMAIL);<br>
                    var_dump($result);<br>
                </code>
                <?php
                $mail = "joe@.com";
                $mail2 = "joe-dev@gmail.com";

                var_dump(filter_var($mail, FILTER_VALIDATE_EMAIL));

                var_dump(filter_var($mail2, FILTER_VALIDATE_EMAIL));
                ?>
            </div>
        </div>

    </div>
    <div class="accordion-item">
        <h2 class="accordion-header" id="headingEight">
            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                Dates
            </button>
        </h2>
        <div id="collapseEight" class="accordion-collapse collapse" aria-labelledby="headingEight">
            <div class="accordion-body">
                <h3>Dates</h3>
                <p>Créer la date du jour au format Jour NumJour Mois Année</p>
                <code>
                    $dayDate = date('l d F Y');<br>
                    echo ($dayDate);<br>
                </code>
                <?php
                $dayDate = date('l d F Y');
                echo ($dayDate);
                ?>
                <p>Timestamp origine</p>
                <code>
                    $originTimestamp = date('l d F Y',0);<br>
                    echo ($originTimestamp);<br>
                </code>
                <?php
                $originTimestamp = date('l d F Y ', 0);
                echo ($originTimestamp);
                ?>
                <p>

            </div>

        </div>
    </div>



</div>

<?php
include_once("block/footer.php");
?>