<?php

$title = "Chaines de caractères";

include_once("../block/header.php");
include_once("../block/navbarAlgo.php");
?>

<h1 class="text-center m-3"><?php echo ($title) ?></h1>
<div>
    <h2 class="m-5 text">Renversez une chaine de caractères</h2>
    <div class="accordion" id="accordionExample">
        <div class="accordion-item">
            <h2 class="accordion-header" id="headingTwo">
                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                    Version 1 Partir de la derniere lettre
                </button>
            </h2>
            <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                <div class="accordion-body">
                    <code>
                        for ($i = ($len - 1); $i >= 0; $i--) {<br>
                        $reversw = $reversw . $word[$i];<br>
                        }<br>
                    </code>
                    <?php
                    $word = "bonjour";
                    $len = mb_strlen($word);
                    $reversw = "";
                    for ($i = ($len - 1); $i >= 0; $i--) {
                        $reversw = $reversw . $word[$i];
                    }
                    var_dump("Partir de la derniere lettre : ", $reversw);
                    ?>
                </div>
            </div>
        </div>
        <div class="accordion-item">
            <h2 class="accordion-header" id="headingOne">
                <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                    Version 2 En partant du dernier caractère
                </button>
            </h2>
            <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                <div class="accordion-body">
                    <code>
                        for ($i = 0; $i < $len; $i++) {<br>
                            $reversw = $word[$i] . $reversw;<br>
                            }<br>
                    </code>
                    <?php

                    for ($i = 0; $i < $len; $i++) {
                        $reversw = $word[$i] . $reversw;
                    }

                    var_dump($reversw);
                    $reversw = "";
                    ?>
                </div>
            </div>
        </div>

        <div class="accordion-item">
            <h2 class="accordion-header" id="headingThree">
                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                    Utilisation des index négatifs
                </button>
            </h2>
            <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                <div class="accordion-body">
                    <code>
                        for ($i = 1; $i <= $len; $i++) {<br>
                            $reversw = $reversw . $word[-$i];<br>
                            }<br>
                    </code>
                    <?php
                    $reversw = "";
                    for ($i = 1; $i <= $len; $i++) {
                        $reversw = $reversw . $word[-$i];
                    }
                    var_dump("Utilisation des index négatifs : ", $reversw);
                    $reversw = "";
                    ?>
                    ?>
                </div>
            </div>
        </div>
        <div class="accordion-item">
            <h2 class="accordion-header" id="headingFour">
                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                    Fonction strrev()
                </button>
            </h2>
            <div id="collapseFour" class="accordion-collapse collapse" aria-labelledby="headingFour" data-bs-parent="#accordionExample">
                <div class="accordion-body">
                    <code>
                        $reversword = strrev($word);<br>
                    </code>
                    <?php
                    $reversword = strrev($word);
                    var_dump("strrev() : " . $reversword);
                    ?>
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="m-5">
    <p>
        Créer une chaine de caractère $message<br>

        Créer une balise
        &lt;p&gt; qui contiendra une boucle for<br>

        Utiliser la boucle pour afficher chaque lettre dans une balise<br>

        <span style="color: black;"> LA_LETTRE </span>

        Utiliser les conditions pour afficher une lettre sur deux en rouge
    </p>
    <div class="accordion" id="accordionExample">
        <div class="accordion-item">
            <h2 class="accordion-header" id="heading7">
                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse7" aria-expanded="true" aria-controls="collapse7">
                    Echo le html complet
                </button>
            </h2>
            <div id="collapse7" class="accordion-collapse collapse" aria-labelledby="heading7" data-bs-parent="#accordionExample">
                <div class="accordion-body">
                    <?php
                    $message = "Bonjour";
                    // Il est préferable de créer une variable pour la longueur
                    // Ca évite de recalculer à chaque tour de boucle
                    $length = strlen($message);
                    for ($i = 0; $i < $length; $i++) {
                        if ($i % 2 === 0) {
                            echo ("<span class='text-danger'> $message[$i] </span>");
                        } else {
                            echo ("<span> $message[$i] </span>");
                        }
                    }

                    ?>
                    <img class="img-fluid p-4" src="../images/correctionSpan.png" alt="correction">

                </div>
            </div>
        </div>
        <div class="accordion-item">
            <h2 class="accordion-header" id="heading11">
                <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapse11" aria-expanded="false" aria-controls="collapse11">
                    Version plus propre en séparant la logique de l'affichage
                </button>
            </h2>
            <div id="collapse11" class="accordion-collapse collapse show" aria-labelledby="heading11" data-bs-parent="#accordionExample">
                <div class="accordion-body">

                    <p>
                        <?php
                        $message = "Bonjour";
                        $length = strlen($message);

                        for ($i = 0; $i < $length; $i++) {

                            if ($i % 2 === 0) {
                                $color = "color: red;";
                            } else {
                                $color = "color: black;";
                            }

                        ?>

                            <span style=' <?php echo ($color) ?> '>


                                <?php echo ($message[$i]) ?>
                            </span>


                        <?php
                        }
                        ?>
                    </p>
                    <img class="img-fluid p-4" src="../images/correctionSpan2.png" alt="correction">

                </div>
            </div>
        </div>
    </div>
</div>








<?php
include_once("../block/footer.php");
?>