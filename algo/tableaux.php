<?php
$title = "Tableaux";

include_once("../block/header.php");
include_once("../block/navbarAlgo.php");
?>
<h1 class="text-center m-3"><?php echo ($title) ?></h1>

<div class="p-5">

    <div class="accordion" id="accordionExample">
        <div class="accordion-item">
            <h2 class="accordion-header" id="headingTwo">
                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                    Opérations tableaux
                </button>
            </h2>
            <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                <div class="accordion-body">

                    <p>Générer un tableau $fruits contenant <br>

                        Pomme, Poire, Banane<br>

                        Afficher le tableau avec var_dump()<br>

                        Devinez les valeurs des variables avec le code suivant<br><br>

                        $fruits[1] = $fruits[2]; valeur fruits[1] ?<br>

                        <code>$fruits[1] vaut "Banane";</code><br>

                        $fruits[2] = $fruits[3]; valeur fruits[2] ?<br>

                        <code>$fruits[2] vaut null; car $fruits[3] n'est pas défini</code>
                    </p>

                    <code>$fruits = ["Pomme", "Poire", null];</code>
                    <?php

                    $fruits = ["Pomme", "Poire", "Banane"];

                    var_dump($fruits);

                    $fruits[1] = $fruits[2];
                    $fruits[2] = $fruits[3];

                    var_dump($fruits);
                    ?>
                </div>
            </div>
        </div>
        <div class="accordion-item">
            <h2 class="accordion-header" id="headingOne">
                <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                    Afficher les valeurs d'un tableaux
                </button>
            </h2>
            <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                <div class="accordion-body">
                    <p>Créer une div class="fruits"</p>
                    <p>Dans cette div, ajouter une balise P pour chaque fruit et afficher sa valeur
                    </p>
                    <code>
                        &lt;div class="fruits" &gt;</br>
                        &lt;?php</br>

                        $iMax = count($fruits);</br>
                        for($i = 0; $i < $iMax; $i++){</br>
                            echo("&lt;p&gt; $fruits[$i] &lt;/p&gt;");</br>
                            }</br>
                            ?&gt;</br>
                            &lt;/div&gt;</br>
                    </code>
                </div>
            </div>
        </div>

        <div class="accordion-item">
            <h2 class="accordion-header" id="headingThree">
                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                    Tableau associatif
                </button>
            </h2>
            <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                <div class="accordion-body">

                    <p>Un tableau pour lequel les indices seront des chaines de caractères</p>
                    <p>Créer un tableau associatif qui représente une voiture contenant : </br>
                        - un modele</br>
                        - une marque</br>
                        - un nombre de sieges</br>
                        - automatique ( bool )</br>
                        - les options ( un tableau de chaines)</p>
                    <?php
                    $car = [
                        "modele" => "Batmobile",
                        "marque" => "Wayne Corp",
                        "nbSieges" => 1,
                        "automatique" => true,
                        "options" => [
                            "clim" => true,
                            "freins" => "Sport",
                            "retroviseurs" => 8
                        ]
                    ];

                    var_dump($car);

                    ?>
                    <div>
                        <h2>
                            <?php echo ($car["modele"]); ?>
                        </h2>
                        <p>
                            <?php echo ($car["marque"]); ?>
                        </p>
                        <p>Nombres de sièges :
                            <?php echo ($car["nbSieges"]); ?>
                        </p>
                        <p>Boite automatique :
                            <?php echo ($car["automatique"] ? "✅" : "❌"); ?>
                        </p>
                        <div>
                            <h3>Options :</h3>
                            <?php
                            foreach ($car["options"] as $key => $option) {
                                echo ("<p>$key : $option</p>");
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="accordion-item">
            <h2 class="accordion-header" id="headingFour">
                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                    Tri de tableaux
                </button>
            </h2>
            <div id="collapseFour" class="accordion-collapse collapse" aria-labelledby="headingFour" data-bs-parent="#accordionExample">
                <div class="accordion-body">
                    <h2>Tri de nombres pairs impairs</h2>
                    <?php
                    $numbers = [];
                    $odds = [];
                    $evens = [];
                    $odds2 = [];
                    $evens2 = [];
                    for ($i = 0; $i < 10; $i++) {
                        //Ajouter au tableau l'entier aleatoire
                        $numbers[$i] = random_int(0, 100);
                    }
                    var_dump($numbers);

                    $len = count($numbers);
                    for ($i = 0; $i < $len; $i++) {
                        if ($numbers[$i] % 2 === 0) {
                            $evens2[] = $numbers[$i];
                        } else {
                            $odds2[] = $numbers[$i];
                        }
                    }
                    foreach ($numbers as $number) {
                        if ($number % 2 === 0) {
                            $evens[] = $number;
                        } else {
                            $odds[] = $number;
                        }
                    }

                    // De nombreux tri sont possibles et different selon le besoin
                    sort($odds);
                    sort($evens);
                    var_dump($odds, $evens);
                    sort($odds2);
                    sort($evens2);
                    var_dump($odds2, $evens2);

                    ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
include_once("../block/footer.php");
?>