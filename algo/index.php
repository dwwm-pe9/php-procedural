<?php

$title = "Algo";

include_once("../block/header.php");
include_once("../block/navbarAlgo.php");
?>

<h1 class="text-center m-3"><?php echo ($title); ?></h1>

<div class="accordion" id="accordionExample">
    <div class="accordion-item">
        <h2 class="accordion-header" id="headingTwo">
            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                Déclarer des variables
            </button>
        </h2>
        <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
            <div class="accordion-body">
                <ul>
                    <li>
                        <div>
                            <h3>Texte (chaine de caracteres / string)</h3>
                            <code>$name = "Gaetan";</code>
                            <p>Déclarer une chaine vide</p>
                            <code>$message = "";</code>
                            <p>Récupérer la longueur d'une chaine : </p>
                            <code>$nameL = strlen($name);</code>
                        </div>
                    </li>
                    <li>
                        <div>
                            <h3>Nombres entier ( int )</h3>
                            <code>$age = 27;</code>
                        </div>
                    </li>
                    <li>
                        <div>
                            <h3>Nombre décimal ( float )</h3>
                            <code>$taille = 175.6;</code>
                        </div>
                    </li>
                    <li>
                        <div>
                            <h3>Booléen</h3>
                            <code>$isHuman = true;</code><br>
                            <code>$gameStarted = false;</code>
                        </div>
                    </li>
                    <li>
                        <div>
                            <h3>Tableaux ( array )</h3>
                            <p>Le tableau est délimité par les <code>[ ]</code><br>Les valeurs sont séparées par des <code>,</code></p>
                            <p>Déclarer un tableau pré-rempli</p>
                            <code>$tab = [1,50,69,78]</code> ou <code>$tab = array(25,65,98,4,36)</code>

                            <p>Déclarer un tableau vide</p>
                            <code>$tab = []</code> ou <code>$tab = array()</code>
                            <p>Chaque valeur possède par défaut un indice numérique commencant par <code>0</code></p>

                            <p>Cibler la valeur du premier indice</p>
                            <code>$tab[0];</code>
                            <p>Stocker la valeur du premier indice</p>
                            <code>$var = $tab[0];</code>
                            <p>Remplacer la valeur du premier indice</p>
                            <code>$tab[0] = "Bonjour";</code>
                            <p>Récupérer la longueur d'un tableau</p>
                            <code>$len = count($tab);</code>
                            <p>Récupérer la valeur du dernier indice</p>
                            <code>$lastIndex = count($tab) - 1</code>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="accordion-item">
        <h2 class="accordion-header" id="headingOne">
            <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                Opérations - Concaténation
            </button>
        </h2>
        <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
            <div class="accordion-body">
                <ul>
                    <li>
                        <p>
                            La concaténation permet "d'ajouter" des chaines de caracteres entre elles
                        </p>
                        <p> En PHP on concaténe avec le <code>.</code> :<br>
                            <code>$message = "Je m'appelle" . $name;</code>
                        </p>
                        <p> Plusieurs syntaxes possibles : <br>
                            Sans point ( pas de calcul possible, espace obligatoire apres variable) : <code>$message = "Je m'appelle $name";</code><br>
                            Sans point avec des accolades autour des variables (optimal): <code>$message = "Je m'appelle {$name}";</code>
                            <br>Résultat :
                            <?php
                            $name = "Gaëtan";
                            $age = 27;
                            $message = "Je m'appelle {$name}";
                            echo ($message);
                            ?>
                        </p>
                        <p> Il est possible de concaténer du texte et des nombre: <br>
                            <code>$message = "Je m'appelle {$name} j'ai {$age} ans";</code>
                        </p>
                        <p>Résultat :
                            <?php
                            $message = "Je m'appelle {$name}, j'ai {$age} ans";
                            echo ($message);
                            ?>
                        </p>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="accordion-item">
        <h2 class="accordion-header" id="headingThree">
            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                Conditions
            </button>
        </h2>
        <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
            <div class="accordion-body">
                <ul>
                    <li>
                        <h3>IF ELSE</h3>
                        <p>Si votre nom contient moins de 5 lettres afficher : "Ton nom contient NB_LETTRE lettres"</p>
                        <p>Sinon : "Tu as un surnom ?"</p>
                        <p>
                            <code> $length = strlen($name); <br>
                                if($length < 5){ <br>
                                    $message = "Ton nom contient $length lettres";<br>
                                    }else{<br>
                                    $message = "Tu as un surnom ?";<br>
                                    }</code>
                        </p>
                        <p>Variante ternaire <br>
                            <code>
                                $message = $length < 5 ? "Ton nom contient $length lettres" : "Tu as un surnom ?" ;<br>
                            </code>
                        </p>
                        <p>Résultat :
                            <?php
                            $length = strlen($name);
                            $message = $length < 5 ? "Ton nom contient $length lettres" : "Tu as un surnom ?";
                            echo ($message); ?>
                        </p>
                    </li>
                    <li>
                        <h3>IF ELSE IF</h3>
                        <p>Si la premiere condition n'est pas validé on peut ajouter d'autres conditions avec <code>else if()</code></p>
                        <p>Si taille est inferieur a 1m40 : afficher "Tu es trop petit pour ce manège"</p>
                        <p>Si taille est superieur a 1m40 et inferieur à 1m90 : afficher "Tu es trop grand pour le manège"</p>
                        <p>Sinon : afficher "Prenez place"</p>
                        <p>
                            <code>
                                $taille = 150;<br>
                                if($taille < 140){<br>
                                    echo("Tu es trop petit pour ce manège");<br>
                                    }else if($taille > 190){<br>
                                    echo("Tu es trop grand pour le manège");<br>
                                    }else{<br>
                                    echo("Prenez place");<br>
                                    }<br>
                            </code>
                        </p>
                        <p>Résultat :
                            <?php
                            $taille = 150;
                            if ($taille < 140) {
                                echo ("Tu es trop petit pour ce manège");
                            } else if ($taille > 190) {
                                echo ("Tu es trop grand pour le manège");
                            } else {
                                echo ("Prenez place");
                            }
                            ?>
                        </p>
                    </li>
                    <li>
                        <h3>Conditions multiples</h3>
                        <p>Permet de valider plusieurs conditions</p>
                        <p>Les deux conditions vraies: <code>if( condition1 && condition2 )</code></p>
                        <p>Au moins une des deux conditions vraie : <code>if( condition1 || condition2 )</code></p>

                        <p>Si vous etes humain et que votre nom contient le lettre G : Afficher " Tu es Gaetan ?"</p>
                        <p>
                            Sinon afficher: "Bonjour VOTRE_NOM"
                        </p>

                        <p>str_contains() renvoie si la chaine contient la lettre / chaine ( booleen ) <br>
                            <code>
                                $containG = str_contains($name, "G");<br>
                                if ($isHuman === true && $containG) {<br>
                                $message = "Tu es Gaetan ?";<br>
                                } else {<br>
                                $message = "Bonjour $name";<br>
                                }<br>
                            </code>
                            <?php

                            $containG = str_contains($name, "G");
                            $isHuman = true;

                            if ($isHuman === true && $containG) {
                                $message = "Tu es Gaetan ?";
                            } else {
                                $message = "Bonjour $name";
                            }
                            ?>

                            Résultat : <?php echo ($message); ?>
                        </p>
                    </li>
                    <li>
                        <p>Version ternaire<br>
                            <code>
                                $containG = str_contains($name, "G");<br>
                                $message = ($isHuman === true && $containG) ? "Tu es Gaetan ?":"Bonjour $name";<br>
                            </code>
                        </p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="accordion-item">
        <h2 class="accordion-header" id="headingFour">
            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                Boucles
            </button>
        </h2>
        <div id="collapseFour" class="accordion-collapse collapse" aria-labelledby="headingFour" data-bs-parent="#accordionExample">
            <div class="accordion-body">
                <ul>
                    <li>
                        <p>Créer un boucle qui tourne 10 fois et qui affiche a chaque tour: "Tour de boucle n° X"</p>

                        <code>
                            for ($i = 1; $i <= 10; $i=$i + 1) { <br>
                                echo("Tour de boucle $i");<br>
                                }<br>
                        </code>
                        <p>Résultat :
                            <?php
                            for ($i = 1; $i <= 10; $i = $i + 1) {
                                echo ("<p>Tour de boucle $i</p>");
                            }

                            ?>
                        </p>
                    </li>
                    <li>
                        <p>Créer un boucle qui affiche la table de multiplication de 5</p>
                        <code>
                            for ($i=0; $i <= 10; $i++) { <br>
                                echo ($i . " x 5 = " . $i*5); <br>
                                } <br>
                        </code>
                        <p>Résultat :
                            <?php
                            for ($i = 0; $i <= 10; $i++) {
                                echo ("<p>" . $i . " x 5 = " . $i * 5 . "</p>");
                            }

                            ?>
                        </p>
                    </li>
                    <li>
                        <p>Créer un boucle qui compte de 100 a 50, deux par deux</p>
                        <code>
                            for ($i = 100; $i >= 50; $i = $i - 2) {<br>
                            echo ($i . "&lt;br&gt;")<br>
                            }
                        </code>
                        <p>
                            <?php
                            for ($i = 100; $i >= 50; $i = $i - 2) {
                                echo ($i . "<br>");
                            }
                            ?>
                        </p>
                    </li>
                    <li>
                        <h3>Foreach</h3>
                        <p>Parcours tout les éléments d'un tableau, du début à la fin et de un en un </p>
                        <code>
                            $animals = ["Dég", "Cat", "Bunny", "Pig", "🐔"];<br>
                            foreach ($animals as $animal) {<br>
                            echo ("&lt;p&gt;$animal&lt;/p&gt;");<br>
                            }<br>

                        </code>
                        <?php
                        $animals = ["Dég", "Cat", "Bunny", "Pig", "🐔"];

                        foreach ($animals as $animal) {
                            echo ("<p>$animal</p>");
                        }

                        ?>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>


<?php
include_once("../block/footer.php");
?>