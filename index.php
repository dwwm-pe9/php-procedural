<?php
include_once("block/header.php");

session_start();

if (isset($_SESSION["username"])) {
    $username = $_SESSION["username"];
    echo ("<h3>Bonjour $username</h3>");
}

?>
<div class="container mt-5">
    <h2>Informations sur l'environnement PHP</h2>
    <table class="table table-bordered">
        <thead class="thead-light">
            <tr>
                <th scope="col">Paramètre</th>
                <th scope="col">Valeur</th>
                <th scope="col">Description</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Version PHP</td>
                <td><?php echo PHP_VERSION; ?></td>
                <td>La version actuelle de PHP installée sur le serveur.</td>
            </tr>
            <tr>
                <td>Version du serveur web</td>
                <td><?php echo $_SERVER['SERVER_SOFTWARE']; ?></td>
                <td>La version du logiciel de serveur web utilisé (par exemple Apache, Nginx).</td>
            </tr>
            <tr>
                <td>Configuration du serveur</td>
                <td><?php echo php_sapi_name(); ?></td>
                <td>Le type d'interface SAPI utilisé par PHP (par exemple, CLI pour l'interface en ligne de commande, CGI pour Common Gateway Interface).</td>
            </tr>
            <tr>
                <td>Extension MYSQLI installée</td>
                <td><?php echo extension_loaded('mysqli') ? 'Oui' : 'Non'; ?></td>
                <td>Permet la connexion à une base de données MySQL.</td>
            </tr>
            <tr>
                <td>Extension PDO installée</td>
                <td><?php echo extension_loaded('pdo') ? 'Oui' : 'Non'; ?></td>
                <td>Permet l'accès aux bases de données via l'interface PDO (PHP Data Objects).</td>
            </tr>
            <tr>
                <td>Extension cURL installée</td>
                <td><?php echo extension_loaded('curl') ? 'Oui' : 'Non'; ?></td>
                <td>Permet d'effectuer des requêtes HTTP et d'autres opérations sur les URL.</td>
            </tr>
            <tr>
                <td>Extension XDEBUG installée</td>
                <td><?php echo extension_loaded('xdebug') ? 'Oui' : 'Non'; ?></td>
                <td>Permet la manipulation d'images et la création de graphiques en PHP.</td>
            </tr>
            <tr>
                <td>Extension JSON installée</td>
                <td><?php echo extension_loaded('json') ? 'Oui' : 'Non'; ?></td>
                <td>Permet de travailler avec des données JSON (JavaScript Object Notation).</td>
            </tr>
            <tr>
                <td>Extension sodium installée ( utile au JWT token )</td>
                <td><?php echo extension_loaded('sodium') ? 'Oui' : 'Non'; ?></td>
                <td>Permet de travailler avec des données JSON (JavaScript Object Notation).</td>
            </tr>
            <tr>
                <td>Extension GD installée</td>
                <td><?php echo extension_loaded('gd') ? 'Oui' : 'Non'; ?></td>
                <td>Permet la manipulation d'images et la création de graphiques en PHP.</td>
            </tr>
        </tbody>
    </table>
</div>


<?php
include_once("block/footer.php");
?>