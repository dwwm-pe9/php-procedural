<?php

$title = "Get";

include_once("../block/header.php");
include_once("../block/navBarTransmission.php");
?>

<div class="container">
    <h1 class="text-center m-3"><?php echo ($title) ?></h1>

    <p class="text-center">
        <code>GET</code> est une méthode <code>HTTP</code>, elle est utile pour demander des données<br>
        Cette méthode permet d'ajouter des données à la fin de l'url <br>
        La méthode <code>GET</code> transmets les données via <code>URL</code>, qui est contenu dans le <code>HEADER</code> de la requete au serveur<br>
        avec la syntaxe <code>?</code> pour renseigner la méthode GET et <code>&</code> pour séparer les données<br>
        EXEMPLE : monUrl.php<code>?inputName1=inputValue1&inputName2=inputValue2</code><br>
        Exemple: https://fr.wikipedia.org/w/index.php<code>?title=Portail:Plantes_x%C3%A9rophytes&action=edit&redlink=1</code>

    </p>

    <h4>Récuperer les données aves la super globale <code>$_GET</code></h4>
    <?php var_dump($_GET) ?>
</div>


<?php
include_once("../block/footer.php");
?>