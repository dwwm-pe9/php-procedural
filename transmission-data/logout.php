<?php
    session_start();

    unset($_SESSION["username"]);
    unset($_SESSION["password"]);

    // Variante mais détruit toute la session, ne pas oublier de la lancer
    // session_destroy();
    header("Location: https://localhost/php-procedural/transmission-data/login.php");
?>