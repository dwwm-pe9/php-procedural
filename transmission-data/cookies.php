<?php


$correctDate =  date_create('now')
    ->format('Y-m-d H:i:s');

setcookie("date", $correctDate, time() + 3600,"/");

setcookie("PHPversion", PHP_VERSION, time() + 3600,"/");

setcookie("userAgent", $_SERVER["HTTP_USER_AGENT"], time() + 3600,"/");

header("Location: index.php");
