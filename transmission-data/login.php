<?php

$title = "Login";

$username = "Admin";
$password = "Admin";

$errors = [];

// est ce que j'ai validé le form
if (
    $_SERVER["REQUEST_METHOD"] === "POST" &&
    isset($_POST["username"], $_POST["password"])
) {
    //est ce que les infos sont correctes
    if ($_POST["username"] === $username && $_POST["password"] === $password) {
       
        //Session save
        session_start();

        $_SESSION["username"] = $_POST["username"];
        $_SESSION["password"] = $_POST["password"];

        header("Location: https://localhost/php-procedural/index.php");
    } else {
        $errors["global"] = "Identifiants invalides";
    }
}


include_once("../block/header.php");
include_once("../block/navBarTransmission.php");
?>

<div class="container">
    <h1 class="text-center m-3"><?php echo ($title) ?></h1>

    <form method="POST" action="login.php">

        <label for="username">Username</label>
        <input type="text" name="username" id="username">
        <label for="password">Password</label>
        <input type="text" name="password" id="password">

        <?php
        if (isset($errors["global"])) {
            echo ("<p class='text-danger'>" .
                $errors["global"] . "</p>");
        }
        ?>
        <input type="submit" value="Valider">
    </form>

    <?php 
        if( !isset($_SESSION)){
            session_start();
        }

        if( isset($_SESSION["username"])){
           include_once("logoutForm.php");
        }
    ?>
</div>


<?php
include_once("../block/footer.php");
?>