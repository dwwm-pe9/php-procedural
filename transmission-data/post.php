<?php
$title = "Post";

include_once("../block/header.php");
include_once("../block/navBarTransmission.php");
?>

<div class="container">
    <h1 class="text-center m-3"><?php echo ($title) ?></h1>
    <p class="text-center">
        <code>POST</code> est une méthode <code>HTTP</code>, elle est utile pour envoyer des données ( formulaires )<br>
        Cette méthode ajoute des données dans le <code>BODY</code> de la requete<br>
    </p>
    <h4>Récuperer les données aves la super globale <code>$_POST</code></h4>
    <?php var_dump($_POST) ?>
    <p>
        Les données sont retrouvables dans l'onglet Network -> Payload de l'inspecteur
    </p>
    <img class="img-fluid" src="../images/postInspect.png" alt="postINspect">
    <img class="img-fluid" src="../images/payload.png" alt="postINspect">

</div>
<?php
include_once("../block/footer.php");
?>