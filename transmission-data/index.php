<?php

var_dump($_COOKIE);

$title = "Transmission de données";

include_once("../block/header.php");
include_once("../block/navBarTransmission.php");

?>

<div class="container">
    <h1 class="text-center m-3"><?php echo ($title) ?></h1>
    <div class="container">
        <h3>Lien vers get.php avec le parametre boisson</h3>
        <a href="get.php?boisson=café">Commande Café</a>
        <a href="get.php?boisson=expresso">Commande Expresso</a>
    </div>
    <div>
        <p class="m-5 px-5">
            Un formulaire est valider grace aux <code>
                < button>
                    < /button> ou < input type="submit">
            </code>
            <br><br>
            Lorsqu'il est valider, il transmets les données contenus dans les inputs
            <br>
            Chaque input aura donc besoin d'un <code>NAME</code> et d'une <code>VALUE</code> pour que la transmission fonctionne correctement
            <br>
            Les données seront transmise dans la page renseigné dans l'attribut action:<br><br>
            <code>
                < form action="index.php">
            </code>
            <br>
            La méthode de transmission sera renseigné dans l'attribut method:<br><br>
            <code>
                < form action="index.php" method="POST">
            </code>
            Possible d'ajouter des paramètres GET,pertinent avec méthode POST, pour conserver / transmettre d'autres données<br>
            <code>
                < form action="index.php?action=edit&id=5" method="POST">
            </code>
        </p>

    </div>

    <div class="container">
        <h3>Formulaire méthode GET action "get.php"</h3>
        <code>
            < form method="GET" action="get.php">
        </code>
        <form class="d-flex justify-content-center align-items-center" method="GET" action="get.php">
            <div class="d-flex m-3 justify-content-center align-items-center">
                <label class="form-label m-2" for="inputName">Name</label>
                
                <input class="form-control" type="text" id="inputName" name="name">
                <input type="hidden" name="hiddenData" value="expresso">
            
            </div>
            <input class="btn btn-primary h-50" type="submit" value="Valider">
        </form>

    </div>
    <div class="container">
        <h3>Formulaire méthode POST action "post.php"</h3>
        <code>
            < form method="POST" action="post.php">
        </code>
        <form class="d-flex align-items-center" method="POST" action="post.php">
            <div class="d-flex m-3 justify-content-center align-items-center">
                <label class="form-label m-2" for="inputName">Name</label>
                <input class="form-control" type="text" id="inputName" name="name">
            </div>
            <input class="btn btn-primary" type="submit" value="Valider">

        </form>

    </div>
</div>
<?php
include_once("../block/footer.php");
?>