<?php
$title = "Functions Replay";
include_once("block/header.php");
?>


<div class="container">

    <h1 class="text-center m-3"><?php echo ($title) ?></h1>

    <ul>
        <li>
            <h2>1 Une fonction qui affiche une balise texte</h2>
            <ul>
                <li>Permettant de renseigner une balise texte (tag) et son contenu</li>
                <?php 

                    function displayTextTag($tag,$content,$cssClasses = "",$id = ""){
                        echo("<$tag id=\"$id\" class=\"$cssClasses\">$content</$tag>");
                    }
                    
                    // Tous les params tag contenu class id
                    displayTextTag("h3","Contenu 1","text-danger","h3Title");
                    // Tag classes tag contenu class 
                    displayTextTag("p","Contenu 2","text-success");
                    // tag contenu
                    displayTextTag("span","Contenu 3");
                    // tag contenu id
                    displayTextTag(tag:"span",content:"Contenu 3",id:"span");
                
                ?>
            </ul>
            <ul>
                <li>Permettre a la fonction d'ajouter ses classes css et un id en options</li>
            </ul>
        </li>
        <li>
        <h2>2 Une fonction qui affiche une balise image</h2>
            <ul>
                <li>
                    Permettant de renseigner le lien, les classes css, le alt, ainsi que id, width et height en option
                </li>
                <?php

                    /**
                     * Display a <img> tag with given parameters
                     * @param string $src The image source path
                     * @param string $width The width of the image "20px", "20%"
                     * @param string $height The height of the image "20px", "20%"
                     * @return void
                     */

                    function displayImageTag(string $src, string $cssClasses = "",string  $id = "",
                    string   $width = "",string  $height = ""): void
                    {

                        echo("<img src=\"$src\" id=\"$id\" class=\"$cssClasses\" width=\"$width\" height=\"$height\" >");

                    }
                    
                    displayImageTag("https://www.php.net/images/logos/php-logo.svg","border","","100px","100px");
                ?>
                
            </ul>
        </li>
    </ul>
    
    <h3>Typer les paramètres/ retour de la fonction</h3>
    <h3>Créer une php DOC</h3>
    


</div>



<?php
include_once("block/footer.php");
?>