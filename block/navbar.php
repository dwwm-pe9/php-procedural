<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container-fluid">

        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
            <li class="nav-item">
                <a class="nav-link" href="https://localhost/php-procedural/index.php">Index</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="https://localhost/php-procedural/algo/index.php">Algo</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="https://localhost/php-procedural/algo/articles.php">Articles</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="https://localhost/php-procedural/functions.php">Functions</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="https://localhost/php-procedural/transmission-data/index.php">Transmission Data</a>
            </li>
        </ul>
    </div>
</nav>