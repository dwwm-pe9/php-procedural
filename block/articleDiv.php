<div class="article col-md-3 col-sm-6 text-center border m-2 pb-2">

    <img src="<?php echo ($article["img"]); ?>" alt="article title" width="150px" height="150px">
    <h3>
        <?php echo ($article["title"]); ?>
    </h3>
    <p>
        <?php echo ($article["prix"]); ?> €
    </p>
    <p>
        <?php echo ($article["type"]); ?>
    </p>

</div>